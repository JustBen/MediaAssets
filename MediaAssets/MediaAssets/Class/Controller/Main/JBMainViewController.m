//
//  JBMainViewController.m
//  MediaAssets
//
//  Created by Yongbin Zhang on 12/26/13.
//  Copyright (c) 2013 JustBen. All rights reserved.
//

#import "JBMainViewController.h"

#import "JBMediaAssets.h"


@interface JBMainViewController ()

@end

@implementation JBMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:imageView];
    
    __block ALAsset *this_asset;
    
    [JBMAGroup loadGroupsWithTypes:(ALAssetsGroupSavedPhotos) assetsFilter:[ALAssetsFilter allPhotos] success:^(NSArray *groups) {
        for (ALAssetsGroup *group in groups) {
            NSLog(@"%@", group);
            
            [JBMAAsset loadAssetsOfAssetGroup:group withSuccess:^(NSArray *assets) {
                for (ALAsset *asset in assets) {
                    NSLog(@"%@", [asset ceationDate]);
                    this_asset = asset;
                }
            }];
        }
        
        imageView.image = [[this_asset representation] image];
    } failure:^(NSError *error) {
        NSLog(@"%@", error);
    }];
}

@end
