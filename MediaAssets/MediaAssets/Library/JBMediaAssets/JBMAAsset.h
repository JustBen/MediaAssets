//
//  JBMAAsset.h
//  MediaAssets
//
//  Created by Yongbin Zhang on 12/25/13.
//  Copyright (c) 2013 JustBen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>


@interface JBMAAsset : NSObject

//  获取assetsGroup中的Assets(ALAsset)
+ (void)loadAssetsOfAssetGroup:(ALAssetsGroup *)assetGroup withSuccess:(void (^)(NSArray *assets))success;

@end
