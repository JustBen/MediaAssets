//
//  ALAssetsGroup+JBMA.h
//  MediaAssets
//
//  Created by Yongbin Zhang on 12/26/13.
//  Copyright (c) 2013 JustBen. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>

@interface ALAssetsGroup (JBMA)

- (UIImage *)posterImageWithScale:(CGFloat)scale;
- (NSString *)name;
- (NSInteger)assetsNumber;

@end
