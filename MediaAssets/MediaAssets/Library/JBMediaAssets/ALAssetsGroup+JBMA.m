//
//  ALAssetsGroup+JBMA.m
//  MediaAssets
//
//  Created by Yongbin Zhang on 12/26/13.
//  Copyright (c) 2013 JustBen. All rights reserved.
//

#import "ALAssetsGroup+JBMA.h"

#import <AssetsLibrary/AssetsLibrary.h>


@implementation ALAssetsGroup (JBMA)

- (UIImage *)posterImageWithScale:(CGFloat)scale
{
    return [UIImage imageWithCGImage:self.posterImage scale:scale orientation:UIImageOrientationUp];
}

- (NSString *)name
{
    return [self valueForProperty:ALAssetsGroupPropertyName];
}

- (NSInteger)assetsNumber
{
    return [self numberOfAssets];
}

@end
