//
//  JBMAAsset.m
//  MediaAssets
//
//  Created by Yongbin Zhang on 12/25/13.
//  Copyright (c) 2013 JustBen. All rights reserved.
//

#import "JBMAAsset.h"

@implementation JBMAAsset

#pragma mark - class methods

//  获取assetsGroup中的Assets(ALAsset)
+ (void)loadAssetsOfAssetGroup:(ALAssetsGroup *)assetGroup withSuccess:(void (^)(NSArray *assets))success
{
    if (assetGroup) {
        NSMutableArray *assets = [[NSMutableArray alloc] init];
        ALAssetsGroupEnumerationResultsBlock resultsBlockOfReload = ^(ALAsset *asset, NSUInteger index, BOOL *stop) {
            if (asset) {
                [assets addObject:asset];
            } else {
                if (success) {
                    success(assets);
                }
            }
        };
        
        [assetGroup enumerateAssetsUsingBlock:resultsBlockOfReload];
    }
}

@end
