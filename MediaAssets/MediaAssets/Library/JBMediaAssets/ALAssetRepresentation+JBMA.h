//
//  ALAssetRepresentation+JBMA.h
//  MediaAssets
//
//  Created by Yongbin Zhang on 12/26/13.
//  Copyright (c) 2013 JustBen. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>

@interface ALAssetRepresentation (JBMA)

- (long long)sizeInM;

- (UIImage *)image;

@end
