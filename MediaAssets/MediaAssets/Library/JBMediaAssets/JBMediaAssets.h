//
//  JBMediaAssets.h
//  MediaAssets
//
//  Created by Yongbin Zhang on 12/26/13.
//  Copyright (c) 2013 JustBen. All rights reserved.
//

#ifndef MediaAssets_JBMediaAssets_h
#define MediaAssets_JBMediaAssets_h

#import "JBMAGroup.h"
#import "JBMAAsset.h"
#import "ALAssetsGroup+JBMA.h"
#import "ALAsset+JBMA.h"
#import "ALAssetRepresentation+JBMA.h"

#endif
