//
//  JBMAGroup.h
//  MediaAssets
//
//  Created by Yongbin Zhang on 12/25/13.
//  Copyright (c) 2013 JustBen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface JBMAGroup : NSObject

//  根据ALAssetsGroupType获取相应的ALAssetsGroup列表
+ (void)loadGroupsWithTypes:(ALAssetsGroupType)types assetsFilter:(ALAssetsFilter *)assetsFilter success:(void (^)(NSArray *groups))success failure:(void (^)(NSError *error))failure;

@end
