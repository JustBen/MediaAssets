//
//  ALAsset+JBMA.h
//  MediaAssets
//
//  Created by Yongbin Zhang on 12/26/13.
//  Copyright (c) 2013 JustBen. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreLocation/CoreLocation.h>


@interface ALAsset (JBMA)

- (UIImage *)thumbnailImage;
- (NSString *)type;
- (CLLocation *)location;
- (NSDate *)ceationDate;

- (ALAssetRepresentation *)representation;

@end
