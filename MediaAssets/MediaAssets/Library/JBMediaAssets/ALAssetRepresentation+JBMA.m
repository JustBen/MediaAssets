//
//  ALAssetRepresentation+JBMA.m
//  MediaAssets
//
//  Created by Yongbin Zhang on 12/26/13.
//  Copyright (c) 2013 JustBen. All rights reserved.
//

#import "ALAssetRepresentation+JBMA.h"

@implementation ALAssetRepresentation (JBMA)

- (long long)sizeInM
{
    return [self size]/ (1024 * 1024);
}

- (UIImage *)image
{
    return [UIImage imageWithCGImage:[self fullResolutionImage]];
}

@end
