//
//  ALAsset+JBMA.m
//  MediaAssets
//
//  Created by Yongbin Zhang on 12/26/13.
//  Copyright (c) 2013 JustBen. All rights reserved.
//

#import "ALAsset+JBMA.h"

@implementation ALAsset (JBMA)

- (UIImage *)thumbnailImage
{
    return [UIImage imageWithCGImage:self.thumbnail];
}

- (NSString *)type
{
    return [self valueForProperty:ALAssetPropertyType];
}

- (CLLocation *)location
{
    return [self valueForProperty:ALAssetPropertyLocation];
}

- (NSDate *)ceationDate
{
    return [self valueForProperty:ALAssetPropertyDate];
}

- (ALAssetRepresentation *)representation
{
    return [self defaultRepresentation];
}

@end
